\documentclass{beamer}

\usetheme{metropolis}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{graphicx}
\usepackage[]{interval}

\usepackage[T1]{fontenc}
\usepackage{lmodern}
\DeclareTextCommandDefault{\nobreakspace}{\leavevmode\nobreak\ }

\newcommand{\xs}{\mathbf{x}}
\newcommand{\us}{\mathbf{u}}
\newcommand{\TT}{\top}
\intervalconfig{soft open fences}

\title{Application de machine learning aux problèmes de robotique
et commande optimale}
\author{Aamr EL KAZDADI\texorpdfstring{\\}{}
Tuteur: Justin CARPENTIER}
\date{}

\begin{document}
\frame{\titlepage}
\frame{\tableofcontents}

\section{Introduction}%
\label{sec:intro}
\begin{frame}[c]{Introduction}
    \textbf{Système dynamique}\\
    État évolue en fonction de l'état et de la commande.\\
    Commande donnée par un agent externe.
    \[
        x_{i+1} = f(x_i, u_i).
    \]
    \begin{center}
        \includegraphics[width=4cm]{plots/invpend.pdf}
    \end{center}
    \pause%
    $f$ peut être inconnue.\\
\end{frame}

\begin{frame}[c]{Introduction}
    \[
        x_{i+1} =\>?
    \]
    \textbf{Objectif}\\
    Estimation de la fonction $f$ à partir de données (trajectoires).\\
    Prédiction/commande de la trajectoire pour accomplir une tâche.\\
\end{frame}

\section{Commande optimale}%
\label{sec:commande_optimale}

\begin{frame}[c]{Notions}
    Horizon temporel fixé, état initial $x_0$ fixé.\\
    Évolution entre $i = 0 \rightarrow i = N$.\\
    \textbf{Trajectoire}
    \begin{align*}
        \us &= (u_0, \dots, u_{N-1}),\\
        \xs &= (x_0, \dots, x_{N-1}, x_N).
    \end{align*}
    \pause%
    \textbf{Fonction coût:} On minimise
    \[
        J(\us) = \sum_{i=0}^{N-1} l_i(x_i, u_i) + l_f(x_N).
    \]
\end{frame}

\begin{frame}[c]{Fonction valeur}
    $\us_i = (u_i, u_{i+1}, \dots, u_{N-1})$ queue de la suite de commandes.\\
    État initial fixé $x_i = x$.\\
    $\xs_i = (x_i=x, x_{i+1}, \dots, x_N)$ queue de la suite des états.\\
    \pause%
    \textbf{Coût restant}
    \[
        J_i(x, \us_i) = \sum_{j=i}^{N-1} l_i(x_j, u_j) + l_f(x_N).
    \]
    \pause%
    \textbf{Fonction valeur}
    \[
        V_i(x) = \min_{\us_i} J_i(x, \us_i).
    \]
\end{frame}

\begin{frame}[c]
    \textbf{Fonction valeur}
    \[
        V_i(x) = \min_{\us_i} J_i(x, \us_i).
    \]
    \pause%
    \textbf{Récurrence}
    \begin{align*}
        V_N(x) &= l_f(x),\\
        V_i(x) &= \min_u l_i(x,u) + V_{i+1}(f(x,u)).
    \end{align*}
    \begin{center}
        Commande optimale $\iff$ Minimisation du RHS pour $i=0$
    \end{center}
\end{frame}

\section{Programmation différentielle dynamique}%
\label{sec:programmation_differentielle_dynamique}
\begin{frame}[c]{Programmation dynamique}
    \[
        V_i(x) = \min_u l_i(x,u) + V_{i+1}(f(x,u)).
    \]
    \textbf{Espace fini}\\
    Explorer tout les états/commandes $\rightarrow$ Calculer les $V_i$
    récursivement.\\\ \\
\end{frame}

\begin{frame}[c]{Programmation différentielle dynamique}
    \textbf{Programmation différentielle dynamique}\\
    Algorithme itératif\\
    Complexité: $O\left(N\times(\dim u^3 + \dim x^2)\right)$ par itération\\
    \begin{itemize}
        \item Trajectoire initiale $(\us, \xs)$\\
        \item Approximer $V_i$ par modèle quadratique, récursivement par backpropagation.
        \item Construire une meilleure trajectoire
    \end{itemize}
    Convergence \textbf{quadratique}
\end{frame}

\begin{frame}[c]{Backward pass}
    \textbf{Étape de base}: $V_N(x) = l_f(x)$\\
    {\textbf{Récurrence}
        \[
            V_i(x) =
            \only<1>{%
                \makebox[0pt][l]{%
                    $\displaystyle\min_u
                    l_i(x,u) + V_{i+1}(f(x,u))$
                }%
                \phantom{\underbracket{\min_u l_i(x,u) + V_{i+1}(f(x,u))}}%
            }
            \only<2>{%
                \makebox[0pt][l]{%
                    $\displaystyle\min_u
                    l_i(x,u) + V_{i+1}(f(x,u))$
                }%
                \makebox[0pt][l]{%
                    $\displaystyle\phantom{\min_u}
                    \underbracket{\phantom{l_i(x,u) + V_{i+1}(f(x,u))}}$
                }%
                \phantom{\underbracket{\min_u l_i(x,u) + V_{i+1}(f(x,u))}}%
            }
            \only<3->{%
                \!\underbracket{\min_u l_i(x,u) + V_{i+1}(f(x,u))}
            }
    \]}
    \vphantom{ab}%
    \only<4>{%
        \textbf{Sortie}: Suite de \textit{feedforward} et \textit{feedback}.%
    }
\end{frame}

\begin{frame}[c]{Forward pass}
    Pas $\alpha \in \interval[open right]{0}{1}$, nouvelle trajectoire:
    \begin{align*}
        \hat x_0 &= x_0, \\
        \hat u_i &= u_i + \alpha k_i + K_i (\hat x_i - x_i),\\
        \hat x_i &= f(\hat x_i, \hat u_i),
    \end{align*}
\end{frame}

\begin{frame}[c]{En pratique}
    \begin{itemize}
        \item Approximer les dérivées du 2\textsuperscript{ème} ordre.
        \item Sous problème quadratique non convexe\\
        $\implies$ régularisation des matrices $Q_i^{uu}$.\\
        \item $\alpha$ choisi par recherche linéaire.
        \item Contraintes $u_{\min} \leq u \leq u_{\max}$ en résolvant des QP.\
    \end{itemize}
\end{frame}

\begin{frame}[c]{Commande en boucle fermée}
    \textbf{En boucle ouverte}\\
    Suite de commandes $\us = (u_0, \dots, u_{N-1})$\\\ \\
    \textbf{En boucle fermée}\\
    Stratégies de commandes $\us = (u_0(x_0), \dots, u_{N-1}(x_{N-1}))$\\
    \pause%
    \textbf{Approximation au 1\textsuperscript{er} ordre}
    \[
        u_i(x) = \hat u_i + K_i (x_i - \hat x_i).
    \]
\end{frame}

\section{Estimation de la dynamique}%
\label{sec:dynamics_estimation}
\begin{frame}[c]{Régression par base de fonctions}
    $\Psi$ vecteur de fonctions\\
    On estime $f(x,u)$ par
    \[
        \hat f(x, u) \approx x + \hat\alpha^\TT \Psi(x,u).
    \]
    $\hat\alpha$ est choisi pour minimiser l'erreur quadratique moyenne.
\end{frame}

\begin{frame}[c]{Régression par noyaux}
    $K$ un noyau stationnaire
    \[
        K(x,y) = k(x-y).
    \]
    On estime $f(x,u)$ par
    \[
        \hat f(x,u) \approx \sum_{i=1}^N K(x,X_i).
    \]
    $\hat\alpha$ est la solution d'un problème de régression linéaire régularisée.
\end{frame}

\begin{frame}[c]{Régression par noyaux}
    En pratique, on choisit une famille paramétrique de noyaux.\\
    Les hyperparamètres et paramètres de régularisation sont choisis par grid search,
    optimisation continue, etc.
\end{frame}

\section{Stabilité et opérateur de Koopman}%
\label{sec:koopman_operator}
\begin{frame}[c]{Propriétés du système}
    Système dynamique
    \[
        x_{i+1} = f(x_i).
    \]
    Propriétés qualitatives du système:
    \begin{itemize}
        \item Stabilité
        \item Points fixes
        \item Cycles limites
        \item \dots
    \end{itemize}
\end{frame}

\begin{frame}[c]{Opérateur de Koopman}
    \textbf{Système linéaire}: $x_{i+1} = Ax_i$\\
    Propriétés qualitatives dépendent des valeurs propres de $A$.
    \pause%
    \textbf{Système non linéaire}: Opérateur de Koopman\\
    \[
        \mathcal K(g)(x) = g(f(x)).
    \]
    Linéaire en dimension infinie.\\
    Propriétés du système liées au spectre de $\mathcal K$.
\end{frame}

\begin{frame}[c]{Opérateur de Koopman}
    \textbf{Système non linéaire}: Opérateur de Koopman\\
    \[
        \mathcal K(g)(x) = g(f(x)).
    \]
    Linéaire en dimension infinie.\\
    On étudie les valeurs propres d'une représentation finie de $\mathcal K$, construite
    à partir des données.
\end{frame}

\section{Application et Résultats}%
\label{sec:results}
\begin{frame}[c]{Base de fonctions}
    \[\psi(x,\theta,v,v_\theta, u) = x^i \sin|\cos(j\theta)v^k v_\theta^l u^m\]
        \centering
        \includegraphics[width=0.8\linewidth]{plots/lstsq.pdf}
\end{frame}

\begin{frame}[c]{Noyaux}
    \[
        k(x, \theta, v, v_\theta, u) = \exp\frac{\cos \theta -
            1}{\sigma_\theta^2}\exp\frac{-\lVert v \rVert^2}{\sigma_v^2}\exp\frac{-\lVert
        v_\theta \rVert^2}{\sigma_{v\theta}^2}\exp\frac{-\lVert u \rVert^2}{\sigma_u^2}.
    \]
    \centering
    \includegraphics[width=0.7\linewidth]{plots/kernel_fig.pdf}
\end{frame}

\begin{frame}[c]{Commande optimale}
        \centering
        \includegraphics{plots/pos_fig.pdf}
\end{frame}

\begin{frame}[c]{Commande optimale}
        \centering
        \includegraphics{plots/vel_fig.pdf}
\end{frame}

\section{Conclusion}%
\label{sec:conclusion}
\begin{frame}[c]{Difficultés}
    \begin{itemize}
        \item Robustesse de l'optimisation de commandes
        \item Estimateurs rapides
        \item Estimateurs des dérivées
    \end{itemize}
\end{frame}

\begin{frame}[c]{Objectifs éventuels}
    \begin{itemize}
        \item Horizon temporel mobile.
        \item Contraintes sur l'état ainsi que la commande
        \item Sparsifier l'estimateur
        \item Symétries du système
        \item État caché (deep learning?)
        \item Étude de stabilité
    \end{itemize}
\end{frame}


\end{document}
